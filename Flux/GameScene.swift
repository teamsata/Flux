//
//  GameScene.swift
//  Flux
//
//  Created by AlbertSamuelMelo on 5/12/16.
//  Copyright (c) 2016 AlbertSamuelMelo. All rights reserved.
//

import SpriteKit

class GameScene: SKScene {
    var thomas: SKSpriteNode?

    override func didMoveToView(view: SKView) {
        // Create space ship sprite.
        thomas = SKSpriteNode(imageNamed: "andar1")
        thomas!.xScale = 0.08
        thomas!.yScale = 0.08
        thomas!.position = CGPointMake(frame.width / 2, frame.height / 2)
        
        // Add the sprite to the scene.
        self.addChild(thomas!)

        
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {

        if let touch:UITouch = touches.first as? UITouch! {// Get sprite's current position (a.k.a. starting point).
        let touchPosition = touch.locationInNode(self)
        
        let moveAction = SKAction.moveTo(CGPoint(x: touchPosition.x, y:frame.height/2), duration: 0.5)
        
        thomas!.runAction(moveAction)
        }
        
    }
   
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
}
